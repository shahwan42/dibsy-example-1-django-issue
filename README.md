# Serving Dibsy.js Example 1 issue demo

## Intro

This project is basically [a view (controller) function](./dibsy/urls.py#L20) that serves the [dibsy.js example 1](./dibsy/templates/example_1.html) with the CSS and JS files included in `style` and `script` tags.

## Prerequisites

- Python
- Django


## Installation

Clone the repo

## Run

```bash
$ cd dibsy-example-1-django-issue

$ python manage.py runserver PORT_NUMBER
```

open http://localhost:PORT_NUMBER

## The issue

Infinite loading, and 500 errors in the console.

![Dibsy.js Django Issu](screenshot/2022-09-11_12-54.png "Result")
